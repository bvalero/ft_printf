#ifndef FT_PUTNBR_FORMAT_COMPUTE_PADDING_H
# define FT_PUTNBR_FORMAT_COMPUTE_PADDING_H

# include <stdbool.h>

# include "number_padding.h"
# include "printf_format.h"

void	ft_putnbr_format_compute_padding(int digits, bool negative,
											t_printf_format *format,
											t_number_padding *padding);

#endif
