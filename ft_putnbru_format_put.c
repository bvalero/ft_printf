#include "ft_putnbru_format_put.h"

#include "ft_putchar.h"

void	ft_putnbru_format_put_spaces_left(int amount, t_printf_format *format,
											t_fd_buffer *fd_buffer)
{
	int cnt;

	if (!format->left_justified
			&& (!format->zero_padding || format->precision >= 0))
	{
		cnt = 0;
		while (cnt < amount)
		{
			ft_putchar(' ', fd_buffer);
			++cnt;
		}
	}
}

void	ft_putnbru_format_put_prefix(uintmax_t n, t_printf_format *format,
										t_fd_buffer *fd_buffer)
{
	if (format->alt_form && format->base == 16 && n != 0)
	{
		ft_putchar('0', fd_buffer);
		if (format->uppercase)
			ft_putchar('X', fd_buffer);
		else
			ft_putchar('x', fd_buffer);
	}
}

void	ft_putnbru_format_put_leading_zeros(int amount, t_printf_format *format,
											t_fd_buffer *fd_buffer)
{
	int cnt;
	
	if (format->zero_padding && !format->left_justified
			&& format->precision < 0)
	{
		cnt = 0;
		while (cnt < amount)
		{
			ft_putchar('0', fd_buffer);
			++cnt;
		}
	}
}

void	ft_putnbru_format_put_extra_digits(int extra_digits,
											t_fd_buffer *fd_buffer)
{
	int cnt;
	
	cnt = 0;
	while (cnt < extra_digits)
	{
		ft_putchar('0', fd_buffer);
		++cnt;
	}
}

void	ft_putnbru_format_put_spaces_right(int amount, t_printf_format *format,
											t_fd_buffer *fd_buffer)
{
	int cnt;

	if (format->left_justified)
	{
		cnt = 0;
		while (cnt < amount)
		{
			ft_putchar(' ', fd_buffer);
			++cnt;
		}
	}
}
