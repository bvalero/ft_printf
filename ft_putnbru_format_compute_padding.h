#ifndef FT_PUTNBRU_FORMAT_COMPUTE_PADDING_H
# define FT_PUTNBRU_FORMAT_COMPUTE_PADDING_H

# include <stdint.h>

# include "number_padding.h"
# include "printf_format.h"

void	ft_putnbru_format_compute_padding(int digits, uintmax_t n,
											t_printf_format *format,
											t_number_padding *padding);

#endif
