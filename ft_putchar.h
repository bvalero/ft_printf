#ifndef FT_PUTCHAR_H
# define FT_PUTCHAR_H

# include "fd_buffer.h"

void	ft_putchar(int c, t_fd_buffer *fd_buffer);

#endif
