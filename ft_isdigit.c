#include "ft_isdigit.h"

int	ft_isdigit(int c)
{
	return (c >= 0x30 && c <= 0x39);
}
