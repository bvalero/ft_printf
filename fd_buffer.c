#include "fd_buffer.h"

#include <unistd.h>

void	flush_write_fd_buffer(t_fd_buffer *fd_buffer)
{
	if ((fd_buffer->mode == _IOFBF || fd_buffer->mode == _IOLBF)
			&& fd_buffer->idx != 0)
	{
		write(fd_buffer->fd, fd_buffer->buf,
				fd_buffer->idx * sizeof(*fd_buffer->buf));
		fd_buffer->idx = 0;
	}
}
