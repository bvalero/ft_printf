CC = clang
CFLAGS = -Wall -Wextra -Werror

NAME = libftprintf.a

SRCS = fd_buffer.c \
       ft_isdigit.c \
       ft_printf.c \
       ft_putchar.c \
       ft_putnbr_format.c \
       ft_putnbr_format_compute_padding.c \
       ft_putnbr_format_put.c \
       ft_putnbru_format.c \
       ft_putnbru_format_compute_padding.c \
       ft_putnbru_format_put.c \
       ft_putstr_format.c

OBJS = $(SRCS:.c=.o)

all: $(NAME)

#TODO dependencies

$(NAME): $(OBJS)
	ar -rc $@ $?

clean:
	rm -f $(OBJS)

fclean: clean
	rm -f $(NAME)

re: fclean all

.PHONY: all clean fclean re
