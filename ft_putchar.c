#include "ft_putchar.h"

#include <unistd.h>

void	ft_putchar(int c, t_fd_buffer *fd_buffer)
{
	unsigned char	c2;

	c2 = (unsigned char)c;
	if (fd_buffer->mode == _IOFBF || fd_buffer->mode == _IOLBF)
	{
		fd_buffer->buf[fd_buffer->idx] = c2;
		++fd_buffer->idx;
		if (fd_buffer->idx == fd_buffer->size
				|| (fd_buffer->mode == _IOLBF && c2 == (unsigned char)'\n'))
			flush_write_fd_buffer(fd_buffer);
	}
	else
		write(fd_buffer->fd, &c2, sizeof(c2));
}
