#ifndef FT_PUTNBRU_FORMAT_H
# define FT_PUTNBRU_FORMAT_H

# include <stdint.h>

# include "fd_buffer.h"
# include "printf_format.h"

# define FT_PUTNBRU_FORMAT_DIGITS_BUF_SIZE 64

void	ft_putnbru_format(uintmax_t n, t_printf_format *format,
							t_fd_buffer *fd_buffer);

#endif
