#include "ft_putnbr_format_compute_padding.h"

static int	ft_putnbr_format_compute_width(int digits, bool negative,
											t_printf_format *format)
{
	int	width;

	width = digits;
	if (negative)
		++width;
	else if (format->plus)
		++width;
	else if (format->space)
		++width;
	return (width);
}

void	ft_putnbr_format_compute_padding(int digits, bool negative,
											t_printf_format *format,
											t_number_padding *padding)
{
	int	width;
	
	if (format->precision >= 0 && format->precision > digits)
		padding->extra_digits = format->precision - digits;
	else
		padding->extra_digits = 0;
	width = ft_putnbr_format_compute_width(digits + padding->extra_digits,
											negative, format);
	if (format->field_width_min > width)
		padding->amount = format->field_width_min - width;
	else
		padding->amount = 0;
}
