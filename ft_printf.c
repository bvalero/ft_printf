#include "ft_printf.h"

#include <stdarg.h>
#include <stdbool.h> //needed?
#include <stdint.h>
#include <stdio.h>

#include <unistd.h>

#include "fd_buffer.h" //needed?
#include "ft_isdigit.h"
#include "printf_format.h" //needed?
#include "ft_putchar.h"
#include "ft_putnbru_format.h"
#include "ft_putnbr_format.h"
#include "ft_putstr_format.h"

int	ft_printf(const char *format, ...)
{
	unsigned char	buf[BUFSIZ];
	t_fd_buffer		fd_buffer;
	va_list			args;
	t_printf_format	format2;
	size_t			cnt;

	fd_buffer.fd = STDOUT_FILENO;
	fd_buffer.mode = _IOLBF;
	fd_buffer.buf = buf;
	fd_buffer.size = sizeof(buf);
	fd_buffer.idx = 0;
	va_start(args, format);
	cnt = 0;
	while (format[cnt] != '\0')
	{
		if (format[cnt] != '%')
		{

			ft_putchar(format[cnt], &fd_buffer);
			++cnt;
			continue ;
		}
		++cnt;
		format2.left_justified = false;
		format2.plus = false;
		format2.space = false;
		format2.alt_form = false;
		format2.zero_padding = false;
		while (true)
		{
			if (format[cnt] == '-')
				format2.left_justified = true;
			else if (format[cnt] == '+')
				format2.plus = true;
			else if (format[cnt] == ' ')
				format2.space = true;
			else if (format[cnt] == '#')
				format2.alt_form = true;
			else if (format[cnt] == '0')
				format2.zero_padding = true;
			else
				break ;
			++cnt;
		}
		format2.field_width_min = 0;
		if (ft_isdigit((unsigned char)format[cnt]))
		{
			while(ft_isdigit((unsigned char)format[cnt]))
			{
		    	format2.field_width_min = format2.field_width_min * 10
											+ (format[cnt] - '0');
				++cnt;
			}
	    }
	    else if (format[cnt] == '*')
		{
			format2.field_width_min = va_arg(args, int);
			if (format2.field_width_min < 0)
			{
				format2.left_justified = true;
		    	format2.field_width_min = -format2.field_width_min;
			}
			++cnt;
	    }
		format2.precision = -1;
		if (format[cnt] == '.') {
			++cnt;
			format2.precision = 0;
			if (ft_isdigit((unsigned char)format[cnt]))
			{
				while(ft_isdigit((unsigned char)format[cnt]))
				{
		    		format2.precision = format2.precision * 10
										+ (format[cnt] - '0');
					++cnt;
				}
	    	}
			else if (format[cnt] == '*')
			{
		    	format2.precision = va_arg(args, int);
				++cnt;
			}
	    }
		format2.len_modifier = e_printf_format_length_modifier_none;
		if (format[cnt] == 'h')
		{
			++cnt;
			if (format[cnt] == 'h')
			{
				format2.len_modifier = e_printf_format_length_modifier_hh;
				++cnt;
			}
			else
				format2.len_modifier = e_printf_format_length_modifier_h;
		}
		else if (format[cnt] == 'l')
		{
			++cnt;
			if (format[cnt] == 'l')
			{
				format2.len_modifier = e_printf_format_length_modifier_ll;
				++cnt;
			}
			else
				format2.len_modifier = e_printf_format_length_modifier_l;
		}
		if (format[cnt] == 'c')
		{
			unsigned char	c;
			c = (unsigned char)va_arg(args, int);
			ft_putchar(c, &fd_buffer);
		}
		else if (format[cnt] == 's')
		{
			char	*s;
			s = va_arg(args, char *);
			ft_putstr_format(s, &format2, &fd_buffer);
		}
		else if (format[cnt] == 'p')
		{
			uintmax_t	n;
			n = (uintmax_t)(uintptr_t)va_arg(args, void *);
			format2.alt_form = true;
			format2.base = 16;
			format2.uppercase = false;
			ft_putnbru_format(n, &format2, &fd_buffer);
		}
		else if (format[cnt] == 'd' || format[cnt] == 'i')
		{
			intmax_t	n;
			if (format2.len_modifier == e_printf_format_length_modifier_hh)
				n = (signed char)va_arg(args, int);
			else if (format2.len_modifier == e_printf_format_length_modifier_h)
				n = (short)va_arg(args, int);
			else if (format2.len_modifier == e_printf_format_length_modifier_l)
				n = va_arg(args, long);
			else if (format2.len_modifier == e_printf_format_length_modifier_ll)
				n = va_arg(args, long long);
			else
				n = va_arg(args, int);
			format2.base = 10;
			format2.uppercase = false;
			ft_putnbr_format(n, &format2, &fd_buffer);
		}
		else if (format[cnt] == 'u')
		{
			uintmax_t	n;
			if (format2.len_modifier == e_printf_format_length_modifier_hh)
				n = (unsigned char)va_arg(args, unsigned int);
			else if (format2.len_modifier == e_printf_format_length_modifier_h)
				n = (unsigned short)va_arg(args, unsigned int);
			else if (format2.len_modifier == e_printf_format_length_modifier_l)
				n = va_arg(args, unsigned long);
			else if (format2.len_modifier == e_printf_format_length_modifier_ll)
				n = va_arg(args, unsigned long long);
			else
				n = va_arg(args, unsigned int);
			format2.base = 10;
			format2.uppercase = false;
			ft_putnbru_format(n, &format2, &fd_buffer);
		}
		else if (format[cnt] == 'x')
		{
			uintmax_t	n;
			if (format2.len_modifier == e_printf_format_length_modifier_hh)
				n = (unsigned char)va_arg(args, unsigned int);
			else if (format2.len_modifier == e_printf_format_length_modifier_h)
				n = (unsigned short)va_arg(args, unsigned int);
			else if (format2.len_modifier == e_printf_format_length_modifier_l)
				n = va_arg(args, unsigned long);
			else if (format2.len_modifier == e_printf_format_length_modifier_ll)
				n = va_arg(args, unsigned long long);
			else
				n = va_arg(args, unsigned int);
			format2.base = 16;
			format2.uppercase = false;
			ft_putnbru_format(n, &format2, &fd_buffer);
		}
		else if (format[cnt] == 'X')
		{
			uintmax_t	n;
			if (format2.len_modifier == e_printf_format_length_modifier_hh)
				n = (unsigned char)va_arg(args, unsigned int);
			else if (format2.len_modifier == e_printf_format_length_modifier_h)
				n = (unsigned short)va_arg(args, unsigned int);
			else if (format2.len_modifier == e_printf_format_length_modifier_l)
				n = va_arg(args, unsigned long);
			else if (format2.len_modifier == e_printf_format_length_modifier_ll)
				n = va_arg(args, unsigned long long);
			else
				n = va_arg(args, unsigned int);
			format2.base = 16;
			format2.uppercase = true;
			ft_putnbru_format(n, &format2, &fd_buffer);
		}
		else if (format[cnt] == '%')
			ft_putchar('%', &fd_buffer);
		else
		{
			if (format[cnt] != '\0')
				ft_putchar(format[cnt], &fd_buffer);
			else
				break ;
		}
		++cnt;
	}
	va_end(args);
	flush_write_fd_buffer(&fd_buffer);
	return (0);
}
