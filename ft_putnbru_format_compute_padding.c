#include "ft_putnbru_format_compute_padding.h"

static int	ft_putnbru_format_compute_width(int digits, uintmax_t n,
											t_printf_format *format)
{
	int	width;

	width = digits;
	if (format->alt_form && format->base == 16 && n != 0)
			width += 2;
	return (width);
}

void	ft_putnbru_format_compute_padding(int digits, uintmax_t n,
											t_printf_format *format,
											t_number_padding *padding)
{
	int	width;
	
	if (format->precision >= 0 && format->precision > digits)
		padding->extra_digits = format->precision - digits;
	else
		padding->extra_digits = 0;
	width = ft_putnbru_format_compute_width(digits + padding->extra_digits, n,
											format);
	if (format->field_width_min > width)
		padding->amount = format->field_width_min - width;
	else
		padding->amount = 0;
}
