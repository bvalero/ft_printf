#include "ft_putstr_format.h"

#include <stddef.h>

#include "ft_putchar.h"

static void	ft_putstr_format_put_spaces_left(int amount,
												t_printf_format *format,
												t_fd_buffer *fd_buffer)
{
	int cnt;

	if (!format->left_justified)
	{
		cnt = 0;
		while (cnt < amount)
		{
			ft_putchar(' ', fd_buffer);
			++cnt;
		}
	}
}

static void	ft_putstr_format_put_spaces_right(int amount,
												t_printf_format *format,
												t_fd_buffer *fd_buffer)
{
	int cnt;

	if (format->left_justified)
	{
		cnt = 0;
		while (cnt < amount)
		{
			ft_putchar(' ', fd_buffer);
			++cnt;
		}
	}
}

void	ft_putstr_format(char *s, t_printf_format *format,
							t_fd_buffer *fd_buffer)
{
	int	s_len;
	int	padding_amount;
	int	cnt;

	if (s == NULL)
		s = "(null)";
	s_len = 0;
	while ((format->precision < 0 || s_len < format->precision)
			&& s[s_len] != '\0')
		++s_len;
	if (format->field_width_min > s_len)
		padding_amount = format->field_width_min - s_len;
	else
		padding_amount = 0;
	ft_putstr_format_put_spaces_left(padding_amount, format, fd_buffer);
	cnt = 0;
	while (cnt < s_len)
	{
		ft_putchar(s[cnt], fd_buffer);
		++cnt;
	}
	ft_putstr_format_put_spaces_right(padding_amount, format, fd_buffer);
}
