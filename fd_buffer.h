#ifndef FD_BUFFER_H
# define FD_BUFFER_H

# include <stddef.h>
# include <stdio.h>

typedef struct s_fd_buffer
{
	int				fd;
	int				mode;
	unsigned char	*buf;
	size_t			size;
	size_t			idx;
}				t_fd_buffer;

void	flush_write_fd_buffer(t_fd_buffer *fd_buffer);

#endif
