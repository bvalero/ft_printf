#include "ft_putnbru_format.h"

#include <stdbool.h>
#include <stddef.h>

#include "ft_putchar.h"
#include "ft_putnbru_format_compute_padding.h"
#include "ft_putnbru_format_put.h"

static size_t	ft_putnbru_format_convert_base(uintmax_t n, char *digits_buf,
												size_t digits_buf_size,
												t_printf_format *format)
{
	static const char	base_digits_lowercase[16] = "0123456789abcdef";
	static const char	base_digits_uppercase[16] = "0123456789ABCDEF";
	const char			*base_digits;

	if (n == 0 && format->precision == 0)
		return (digits_buf_size);
	if (format->uppercase)
		base_digits = base_digits_uppercase;
	else
		base_digits = base_digits_lowercase;
	while (true)
	{
		--digits_buf_size;
		digits_buf[digits_buf_size] = base_digits[n % format->base];
		n /= format->base;
		if (n == 0)
			break ;
	}
	return (digits_buf_size);
}

static void	ft_putnbru_format_put_digits(char *digits_buf,
											size_t digits_buf_size,
											size_t digits_buf_start_idx,
											t_fd_buffer *fd_buffer)
{
	while (digits_buf_start_idx != digits_buf_size)
	{
		ft_putchar(digits_buf[digits_buf_start_idx], fd_buffer);
		++digits_buf_start_idx;
	}
}

void	ft_putnbru_format(uintmax_t n, t_printf_format *format,
							t_fd_buffer *fd_buffer)
{
	char				digits_buf[FT_PUTNBRU_FORMAT_DIGITS_BUF_SIZE];
	size_t				digits_buf_start_idx;
	t_number_padding	padding;

	digits_buf_start_idx = ft_putnbru_format_convert_base(n, digits_buf,
															sizeof(digits_buf),
															format);
	ft_putnbru_format_compute_padding((int)(sizeof(digits_buf)
											- digits_buf_start_idx),
										n, format, &padding);
	ft_putnbru_format_put_spaces_left(padding.amount, format, fd_buffer);
	ft_putnbru_format_put_prefix(n, format, fd_buffer);
	ft_putnbru_format_put_leading_zeros(padding.amount, format, fd_buffer);
	ft_putnbru_format_put_extra_digits(padding.extra_digits, fd_buffer);
	ft_putnbru_format_put_digits(digits_buf, sizeof(digits_buf),
								digits_buf_start_idx, fd_buffer);
	ft_putnbru_format_put_spaces_right(padding.amount, format, fd_buffer);
}
