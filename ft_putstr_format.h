#ifndef FT_PUTSTR_FORMAT_H
# define FT_PUTSTR_FORMAT_H

# include "fd_buffer.h"
# include "printf_format.h"

void	ft_putstr_format(char *s, t_printf_format *format,
							t_fd_buffer *fd_buffer);

#endif
