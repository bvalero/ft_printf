#ifndef FT_PUTNBR_FORMAT_PUT_H
# define FT_PUTNBR_FORMAT_PUT_H

# include <stdbool.h>

# include "fd_buffer.h"
# include "printf_format.h"

void	ft_putnbr_format_put_spaces_left(int amount, t_printf_format *format,
											t_fd_buffer *fd_buffer);
void	ft_putnbr_format_put_sign(bool negative, t_printf_format *format,
									t_fd_buffer *fd_buffer);
void	ft_putnbr_format_put_leading_zeros(int amount, t_printf_format *format,
											t_fd_buffer *fd_buffer);
void	ft_putnbr_format_put_extra_digits(int extra_digits,
											t_fd_buffer *fd_buffer);
void	ft_putnbr_format_put_spaces_right(int amount, t_printf_format *format,
											t_fd_buffer *fd_buffer);

#endif
