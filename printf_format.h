#ifndef PRINTF_FORMAT_H
# define PRINTF_FORMAT_H

# include <stdbool.h>

typedef enum e_printf_format_length_modifier
{
	e_printf_format_length_modifier_hh,
	e_printf_format_length_modifier_h,
	e_printf_format_length_modifier_none,
	e_printf_format_length_modifier_l,
	e_printf_format_length_modifier_ll
}				t_printf_format_length_modifier;

typedef struct s_printf_format
{
	bool							left_justified;
	bool							plus;
	bool							space;
	bool							alt_form;
	bool							zero_padding;
	int								field_width_min;
	int								precision;
	t_printf_format_length_modifier	len_modifier;

	int								base;
	bool							uppercase;
}				t_printf_format;

#endif
