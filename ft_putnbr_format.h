#ifndef FT_PUTNBR_FORMAT_H
# define FT_PUTNBR_FORMAT_H

# include <stdint.h>

# include "fd_buffer.h"
# include "printf_format.h"

# define FT_PUTNBR_FORMAT_DIGITS_BUF_SIZE 64

void	ft_putnbr_format(intmax_t n, t_printf_format *format,
							t_fd_buffer *fd_buffer);

#endif
